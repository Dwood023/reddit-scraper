#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate lazy_static;
#[macro_use] extern crate rocket;

use reqwest::Client;
use serde_json::Value;
use image::{GenericImageView, Pixel};
use rand::Rng;

use cgmath::{BaseNum, InnerSpace, Vector4};

type Color = Vector4<u8>;

struct Cluster {
    center: Color,
    children: Vec<Color>
}

impl Cluster {
    pub fn new(center: Color) -> Cluster {
        Cluster {
            center,
            children: vec!()
        }
    }
    pub fn distance_to(&self, color: Color) -> f32 {
        let diff = color - self.center;

        // Couldn't find a better way of casting between vector types 
        let diff: Vector4::<f32> = Vector4::from([
            diff.x as f32,
            diff.y as f32,
            diff.z as f32,
            diff.w as f32
        ]);
        diff.magnitude()
    }
    pub fn add(&mut self, color: Color) {
        self.children.push(color);
    }
}

fn main() {

    // TODO - Get subject from request argument
    let subject = "cats";
    let url = format!("https://api.reddit.com/search?q={}&sort=relevance&limit=100", subject);
    
    // Scrape reddit for the most commented posts related to that subject.
    let client = Client::new();

    let mut response = client.get(&url).send();

    match &mut response {
        Ok(response) => {

            let val: Value = serde_json::from_str(&response.text().unwrap())
                .unwrap();

            let children = &val["data"]["children"];

            if let Value::Array(children) = children {

                let children = children.iter()
                    .map(|post| &post["data"])
                    .filter(|post| &post["domain"] == "i.redd.it")
                    .map(|post| post["url"].as_str().unwrap())
                    .take(3);

                for image in children {
                    // println!("{:#?}", image);
                    if let Ok(mut response) = client.get(image).send() {

                        // let bytes: &[u8] = response.bytes().into();

                        println!("Getting image: {}", image);

                        let mut buffer = Vec::new();
                        response.copy_to(&mut buffer);
                        let image = image::load_from_memory(&buffer).unwrap();

                        // Get pixels
                        // Get k random pixels
                        // Use each of these as a starting cluster
                        // Iterate pixels, push to cluster with smallest distance - sqrt(r^2 + g^2 + b^2) 

                        // How many clusters?
                        const K: i32 = 3;

                        let mut clusters = vec!();
                        let mut rng = rand::thread_rng();

                        for _ in 0..K {

                            let (random_x, random_y) = (
                                rng.gen_range(0, image.width()),
                                rng.gen_range(0, image.height())
                            );
                            let random_pixel = image.get_pixel(random_x, random_y);

                            let color = Color::from(random_pixel.channels4());

                            clusters.push(Cluster::new(color));
                        }

                        for (x, y, val) in image.pixels() {
                            let mut closest_i = 0;
                            let color = Color::from(val.channels4());

                            // Great, floats don't imlement Ord trait
                            clusters.iter().min_by_key(|cluster| (**cluster).distance_to(color));
                        }

                    }
                }
            }

        },
        Err(_) => {
            println!("Request failed!");
        }
    }
    // Get post image

    // server::server().launch();
}
use scraper::{Html, Selector};
use regex::Regex;

pub fn extract_links<'a>(body: &'a str) -> impl Iterator<Item = String> {
    let doc = Html::parse_document(&body);
    let selector = Selector::parse("a").unwrap();

    let mut output = vec!();

    for e in doc.select(&selector) {
        if let Some(href) = e.value().attr("href") {
            output.push(href.to_string());
        }
    }

    output.into_iter()
}
pub fn is_local_link(link: &str) -> bool {
    lazy_static! { 
        static ref LINK_WITHIN_DOMAIN: Regex = 
            Regex::new(r"^[^http][\w\-/]+").unwrap(); 
    }

    LINK_WITHIN_DOMAIN.is_match(link) && 
        !link.contains(".") // Ahahaha 
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn extract_links() {
        let fragment = r#"
            <h2>Links</h2>
            <a href="/pages/contact"></a>
            <a href="/somewhere-else"></a>
            <a href="/home"></a>
            <a href="google.com"></a>
        "#;

        let actual: Vec<_> = super::extract_links(fragment)
            .collect();

        let hrefs = [
            "/pages/contact",
            "/somewhere-else",
            "/home",
            "google.com"
        ];

        let expected: Vec<_> = hrefs.into_iter()
            .map(|s| s.to_string())
            .collect();

        assert_eq!(actual, expected);
    }
    #[test]
    fn is_local_link() {
        let local_links = [
            "/about",
            "news",
            "/somewhere-else",
        ];
        for link in local_links.iter() {
            assert!(super::is_local_link(link))
        }
        let not_local = [
            "google.com",
            "https://example.com"
        ];
        for link in not_local.iter() {
            assert!(!super::is_local_link(link))
        }
    }
}
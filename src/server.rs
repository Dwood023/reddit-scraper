use std::sync::{Mutex, Arc};

#[get("/image/<subject>/<rgb>")]
fn images(subject: String, rgb: String) -> String {
    "2".to_string()
}

pub fn server() -> rocket::Rocket {
    rocket::ignite()
        .mount("/", routes![images])
}